from django.contrib import admin

from .models import Customer,Inventory,Order,Entry,Cart,CartEntry

admin.site.register(Customer)
admin.site.register(Inventory)
admin.site.register(Order)
admin.site.register(Entry)
admin.site.register(Cart)
admin.site.register(CartEntry)