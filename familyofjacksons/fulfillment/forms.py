from django.forms import ModelForm
from django import forms
from .models import Customer, Inventory, Order, Entry

class CustomerForm(ModelForm):
    class Meta:
        model   = Customer
        fields  = ["customer_fname", "customer_lname",
                   "customer_email", "customer_phone",
                   "customer_comment"]

class ProductForm(ModelForm):
    class Meta:
        model   = Inventory
        fields  = ["product_code", "product_name",
                   "product_description", "product_qty",
                   "product_cost", "product_discontinued"]

class OrderForm(ModelForm):
    class Meta:
        model   = Order
        fields  = ["order_comment", #"order_customer",
                   "order_due"]

class OrderEntry(ModelForm):
    class Meta:
        model   = Entry
        fields  = ["entry_product", "entry_qty",
                   "entry_comment", "entry_order"]

class CartAddForm(forms.Form):
    product_id    = forms.IntegerField()
    product_qty     = forms.IntegerField(max_value=10, min_value=0)
