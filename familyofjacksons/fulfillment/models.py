from django.db import models
from phone_field import PhoneField

class Customer(models.Model):
    customer_fname  = models.CharField(max_length=60, help_text="First Name of customer.", verbose_name="First Name")
    customer_lname  = models.CharField(max_length=60, help_text="Last Name of customer.", verbose_name="Last Name")
    customer_email  = models.EmailField(null=False, blank=True, help_text="(OPTIONAL) Email address of the customer. Not currently in use.", verbose_name="Email Address")
    customer_phone  = PhoneField(help_text="Phone number of customer", verbose_name="Phone Number")
    customer_comment    = models.CharField(null=False, blank=True, max_length=400, help_text="(OPTIONAL) Use at your descression.", verbose_name="Comment")
    
    def __str__(self):
        return self.customer_fname + " " + self.customer_lname

class Inventory(models.Model):
    product_code    = models.AutoField(primary_key=True, unique=True, help_text="Product code. Can be used for barcodes if desired.", verbose_name="Product Code")
    product_name    = models.CharField(max_length=50, help_text="Short name for product. Should be brief and descriptive.", verbose_name="Product Name")
    product_description = models.CharField(max_length=400, blank=True, null=False, help_text="(OPTIONAL) Description of the product.", verbose_name="Product Description")
    product_qty     = models.IntegerField(help_text="How many of the product is in stock. -1 and lower means quantity is not recorded.", verbose_name="Quantity")
    product_cost    = models.DecimalField(max_digits=6, decimal_places=2, help_text="Cost of the product in USD.")
    product_discontinued    = models.BooleanField(default=False, help_text="Whether the product is discontinued or not.")

    def __str__(self):
        return self.product_name

class Order(models.Model):
    order_date      = models.DateTimeField(auto_now_add=True, help_text="Date and time of the order being placed.")
    order_comment   = models.CharField(max_length=400, blank=True, null=False, help_text="(OPTIONAL) Use at your descression.")
    order_customer  = models.ForeignKey(Customer, on_delete=models.PROTECT, help_text="Customer who placed the order.")
    order_due       = models.DateField(blank=True, null=True, help_text="(OPTIONAL) Date on which the order is due.")

    def __int__(self):
        return self.id

class Cart(models.Model):
    order_date      = models.DateTimeField(auto_now_add=True, help_text="Date and time of the order being placed.")
    def __int__(self):
        return self.id

class Entry(models.Model):
    entry_product   = models.ForeignKey(Inventory, on_delete=models.PROTECT, help_text="Product ordered. ")
    entry_qty       = models.IntegerField(help_text="How many of the product was ordered.")
    entry_comment   = models.CharField(max_length=400, blank=True, help_text="(OPTIONAL) Use at your descression.")
    entry_order     = models.ForeignKey(Order, on_delete=models.PROTECT, help_text="Order to which the entry is attached")

    def __int__(self):
        return self.id

class CartEntry(models.Model):
    product   = models.ForeignKey(Inventory, on_delete=models.PROTECT, help_text="Product ordered. ")
    qty       = models.IntegerField(help_text="How many of the product was ordered.")
    comment   = models.CharField(max_length=400, blank=True, help_text="(OPTIONAL) Use at your descression.")
    cart     = models.ForeignKey(Cart, on_delete=models.PROTECT, help_text="Order to which the entry is attached")

    def __int__(self):
        return self.id
