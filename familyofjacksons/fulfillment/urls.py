from django.urls import include, path

from . import views

urlpatterns = [
    path("", views.inventory, name="inventory"),
    path("customers", views.customers, name="customers"),
    path("customer/<int:customer_id>", views.customer, name="customer"),
    path("customer/<int:customer_id>/edit", views.customer_edit, name="customer-edit"),
    path("inventory", views.inventory, name="inventory"),
    path("product/<int:product_code>", views.product, name="product"),
    path("product/cartadd", views.product_cartadd, name="product_cartadd"),
    path("orders", views.orders, name="orders"),
    path("order/<int:order_id>", views.order, name="order"),
    path("order/<int:order_id>/edit", views.order_edit, name="order-edit"),
    path("cart", views.cart, name="cart"),
    path("cart/order", views.cart_order, name="cart-order"),
    path('accounts/', include('django.contrib.auth.urls')),
]
