from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.urls import reverse

from .models import Customer, Inventory, Order, Entry, CartEntry, Cart
from .forms import *
from .seeother import HttpResponseSeeOther

def index(request):
    return HttpResponse("Hello, World!")

def customers(request):
    customers   = Customer.objects.all()
    context     = {"customers": customers}
    return render(request, "fulfillment/customers.html", context)

def customer(request, customer_id):
    customer    = Customer.objects.get(id=customer_id)
    orders      = Order.objects.filter(order_customer=customer).order_by("order_date")
    entries     = []

    for order in orders:
        for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
            entries.append(entry)

    context     = {"customer": customer, "orders": orders, "entries": entries}
    return render(request, "fulfillment/customer.html", context)

def customer_edit(request, customer_id):
    customer    = Customer.objects.get(id=customer_id)
    orders      = Order.objects.filter(order_customer=customer).order_by("order_date")

    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            fname   = form.cleaned_data["customer_fname"]
            lname   = form.cleaned_data["customer_lname"]
            email   = form.cleaned_data["customer_email"]
            phone   = form.cleaned_data["customer_phone"]
            comment = form.cleaned_data["customer_comment"]

            customer.customer_fname     = fname
            customer.customer_lname     = lname
            customer.customer_email     = email
            customer.customer_phone     = phone
            customer.customer_comment   = comment
            customer.save()
            
            return HttpResponseSeeOther( reverse("customer", args=[customer_id]))
        else:
            return HttpResponseBadRequest("Invaliad request recieved.")
    else:
        form = CustomerForm()
        entries     = []

        for order in orders:
            for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
                entries.append(entry)

        context     = {"customer": customer, "orders": orders, "entries": entries, "form": form}
        return render(request, "fulfillment/customer-edit.html", context)

def inventory(request):
    products    = Inventory.objects.all()
    context     = {"products": products}
    return render(request, "fulfillment/inventory.html", context)

def product(request, product_code):
    product     = Inventory.objects.get(product_code=product_code) 
    context     = {"product": product}
    return render(request, "fulfillment/product.html", context)

def product_cartadd(request):
    if request.method == "POST":
        form = CartAddForm(request.POST)
        if form.is_valid():
            product_id  = form.cleaned_data["product_id"]
            product_qty = form.cleaned_data["product_qty"]

            try:
                if request.session["cart"] != None:
                    cartentry = CartEntry(
                        product=Inventory.objects.get(product_code=product_id),
                        qty=product_qty,
                        cart=Cart.objects.get(id=request.session["cart"])
                    )
                    cartentry.save()
                else:
                    cart = Cart()
                    cart.save()
                    request.session["cart"] = cart.id
                    cartentry = CartEntry(product=Inventory.objects.get(product_code=product_id),
                        qty=product_qty,
                        cart=Cart.objects.get(id=cart.id)
                    )
                    cartentry.save()
            except KeyError:
                cart = Cart()
                cart.save()
                request.session["cart"] = cart.id
                cartentry = CartEntry(product=Inventory.objects.get(product_code=product_id),
                        qty=product_qty,
                        cart=Cart.objects.get(id=cart.id)
                )
                cartentry.save()
                pass
            return HttpResponse("Added to cart")
        else:
            return HttpResponse("Invaliad data")
    else:
        return HttpResponse("Invaliad method")

def orders(request):
    orders      = Order.objects.all()
    entries     = []
    for order in orders:
        for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
            entries.append(entry)

    context     = {"orders": orders, "entries": entries}
    return render(request, "fulfillment/orders.html", context)
def order(request, order_id):
    order      = Order.objects.get(id=order_id)
    entries     = []
    for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
        entries.append(entry)

    context     = {"order": order, "entries": entries}
    return render(request, "fulfillment/order.html", context)

def order_edit(request, order_id):
    order       = Order.objects.get(id=order_id)
    if request.method == "POST":
        form = OrderForm(request.POST)
        if form.is_valid():
            due     = form.cleaned_data["order_due"]
            #lname   = form.cleaned_data["customer_lname"]
            #email   = form.cleaned_data["customer_email"]
            #phone   = form.cleaned_data["customer_phone"]
            #comment = form.cleaned_data["customer_comment"]

            #customer.customer_fname     = fname
            #customer.customer_lname     = lname
            #customer.customer_email     = email
            #customer.customer_phone     = phone
            #customer.customer_comment   = comment
            #customer.save()
            
            return HttpResponse("Due date: ")# + "\nLName: " + lname + "\nEmail: " + email + "\nPhone: " + phone.formatted + "Comment: " + comment)
            #return HttpResponseSeeOther( reverse("customer", args=[customer_id]))
        else:
            entries     = []
            for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
                entries.append(entry)

            context     = {"order": order, "entries": entries, "form": form}
            return render(request, "fulfillment/order-edit.html", context)

    else:
        form = OrderForm()
        entries     = []

        for entry in Entry.objects.filter(entry_order=order).order_by("entry_product"):
            entries.append(entry)

        context     = {"order": order, "entries": entries, "form": form}
        return render(request, "fulfillment/order-edit.html", context)

def cart(request):
    try:
        if request.session["cart"] != None:
            cart    = Cart.objects.get(id=request.session["cart"])
            entries = []
            for entry in CartEntry.objects.filter(cart=cart).order_by("product"):
                entries.append(entry)
            
                context     = {"cart": cart, "entries": entries}
            return render(request, "fulfillment/cart.html", context)
        else:
            return HttpResponse("Cart not defined.")
    except KeyError:
        return HttpResponse("No items in cart.")

def cart_order(request):
    try:
        if request.session["cart"] != None:
            cart = Cart.objects.get(id=request.session["cart"])
            entries = []

            for entry in CartEntry.objects.filter(cart=cart).order_by("product"):
                entries.append(entry)
            return HttpResponse("Not implmented")
        else:
            return HttpResponse("No cart")
    except KeyError:
        return HttpResponse("No cart")